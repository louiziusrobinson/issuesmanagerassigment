// JavaScript source code

$('#button-register').click(() => {
    var info = {};

    info.fullname = $('#user-name').val();
    info.username = $('#create-user').val();
    info.email = $('#user-email').val();
    info.password = $('#create-pass').val();

    var userinfo = JSON.parse(localStorage.getItem('info'));

    if (info.fullname != '' || info.username != '' || info.email != '' || info.password != '') {
        if (userinfo == null) {
            var userinfo = [];
        }

        userinfo.push(info);
        localStorage.setItem('info', JSON.stringify(userinfo))
        document.getElementById("user-name").value = '';
        document.getElementById("create-user").value = '';
        document.getElementById("user-email").value = '';
        document.getElementById("create-pass").value = '';
        window.open('loginPage.html', '_self')
    }
    else {
        document.getElementById("user-name").value = '';
        document.getElementById("create-user").value = '';
        document.getElementById("user-email").value = '';
        document.getElementById("create-pass").value = '';
        swal("Error", "Please Fill in the Boxes", "error")
    }
});

 

$('#button-login').click(() => {
    var username = $('#user-user').val();
    var password = $('#user-pass').val();
    var found = false
    var userinfo = JSON.parse(localStorage.getItem('info'));


    if (userinfo == null) {
        swal("Error", "This User is Found", "error")
    }
    else if (username != '' || password != '') {
        for (var i = 0; i < userinfo.length; i++) {
            if (username == userinfo[i].username && password == userinfo[i].password) {
                localStorage.setItem('UserLogged', JSON.stringify(userinfo[i]))
                found = true;
                break;
            }
        }
        if (found) {
            window.open('issuesManagerPage.html', '_self');
            displayName();
        }
        else {
            swal("Error", "This User is Found", "error")
        }
    }
    else {
        swal("Error", "Please Fill in the Boxes", "error")
    }
});

function displayName() {
    var userinfo = JSON.parse(localStorage.getItem('UserLogged'));
    $('#user-log').html(userinfo.fullname)
}

$('#login-out').click(() => {
    localStorage.removeItem('UserLogged');
    window.open('loginPage.html', '_self');
});

$('#add-issue').click(() => {
    var issue = {};
    var userinfo = JSON.parse(localStorage.getItem('UserLogged'));
    var newissue = JSON.parse(localStorage.getItem(userinfo.fullname));

-0
    issue.number = Math.floor((Math.random() * 500) + 100);;
    issue.status = 'Open'
    issue.description = $('#describe-issue').val();
    issue.severity = $('#severity').val();
    issue.assigned = $('#assigned-to').val();

    if (issue.description == '' || issue.assigned == '') {
        swal("Error", "Please Fill in the Boxes", "error")
    }
    else {
        if (newissue == null) {
            newissue = [];
        }
        newissue.push(issue);
        localStorage.setItem(userinfo.fullname, JSON.stringify(newissue));
        displayIssue();
    }
});

function displayIssue() {
    
    var userinfo = JSON.parse(localStorage.getItem('UserLogged'));
    var newissue = JSON.parse(localStorage.getItem(userinfo.fullname));
    var show = ``;
    

    for (var i = 0; i < newissue.length; i++) {
        show += 
            `<div class="jumbotron jumbotron-fluid page" style='width:1110px' id='page${i+1}'>\
            <div class="container">`
        show +=`<h6>Issue ID: ${newissue[i].number}</h6>\
            <p class="lead">${newissue[i].status}</p>\
            <h5 class="display-4">${newissue[i].description}</h5>\
            <p class="lead">${newissue[i].severity}</p>\
            <p class="lead">${newissue[i].assigned}</p>\
            <button class="btn btn-warning" onclick="closeItem(${i})">Close</button>\
            <button class="btn btn-danger" onclick="deleteItem(${i})">Delete</button>\
            </div>\
            </div>`
    }
    
    $('#showIssue').html(show);

    $('#pagination-demo').twbsPagination('destroy');

    $('#pagination-demo').twbsPagination({
        totalPages: newissue.length,
        // the current page that show on start
        startPage: 1,

        // maximum visible pages
        visiblePages: 5,

        // Text labels
        first: 'First',
        prev: 'Previous',
        next: 'Next',
        last: 'Last',

    // callback function
        onPageClick: function (event, page) {
            $('.page-active').removeClass('page-active');
            $('#page' + page).addClass('page-active');
        },
    });
}

function closeItem(i) {
    var userinfo = JSON.parse(localStorage.getItem('UserLogged'));
    var newissue = JSON.parse(localStorage.getItem(userinfo.fullname));
    if (newissue[i].status == 'Open') {
        newissue[i].status = 'Close';
    }
    localStorage.setItem(userinfo.fullname, JSON.stringify(newissue));
    displayIssue();
}

function deleteItem(i){
    var userinfo = JSON.parse(localStorage.getItem('UserLogged'));
    var newissue = JSON.parse(localStorage.getItem(userinfo.fullname));
    newissue.splice(i, 1)
    localStorage.setItem(userinfo.fullname, JSON.stringify(newissue));
    displayIssue();
}


